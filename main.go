package main

import "fmt"
import "flag"
import "strings"
import "net/url"
import "net/http"

var root string
var history = make(map[string]int)

func check(link, from *url.URL) {
	path := link.String()
	if history[path] != 0 {
		return
	}
	history[path] = 1
	dat, err := http.Get(path)
	if err != nil {
		fmt.Println("ERROR:", err)
		fmt.Println("FROM:", from.String())
		fmt.Println()
		return
	}
	if dat.StatusCode >= 400 {
		fmt.Println("DEAD:", path)
		fmt.Println("FROM:", from.String())
		fmt.Println()
	} else {
		if strings.Contains(link.String(), root) {
			buf := make([]byte, 100000)
			siz, err := dat.Body.Read(buf)
			dat.Body.Close()
			if err != nil {
				fmt.Println("ERROR:", err)
				fmt.Println("FROM:", from.String())
				fmt.Println()
				return
			}
			const blank = 0
			const comment = 1
			const script = 2
			const href = 3
			const equals = 4
			const single = 5
			const double = 6
			var status int = 0
			var strStart int
			for i:=0; i<siz; i++ {
				switch status {
				case blank:
					if string(buf[i:i+4]) == "<!--" {
						status = comment
					} else if string(buf[i:i+8]) == "<script>" {
						status = script
					} else if string(buf[i:i+4]) == "href" {
						status = href
					}
					break
				case comment:
					if string(buf[i:i+3]) == "-->" {
						status = blank
					}
					break
				case script:
					if string(buf[i:i+9]) == "</script>" {
						status = blank
					}
					break
				case href:
					if buf[i] == '=' {
						status = equals
					}
					break
				case equals:
					switch buf[i] {
					case '"':
						status = double
						strStart = i + 1
						break
					case '\'':
						status = single
						strStart = i + 1
						break
					}
					break
				case single:
					if buf[i] == '\'' {
						status = blank
						next, err := link.Parse(string(buf[strStart:i]))
						if err != nil {
							fmt.Println("ERROR:", err)
							fmt.Println("FROM:", from.String())
							fmt.Println()
						} else {
							if strings.Contains(next.Scheme, "http") {
								check(next, link)
							}
						}
					}
					break
				case double:
					if buf[i] == '"' {
						status = blank
						next, err := link.Parse(string(buf[strStart:i]))
						if err != nil {
							fmt.Println("ERROR:", err)
							fmt.Println("FROM:", from.String())
							fmt.Println()
						} else {
							if strings.Contains(next.Scheme, "http") {
								check(next, link)
							}
						}
					}
					break
				}
			}
		} else {
			dat.Body.Close()
		}
	}
}

func main() {
	flag.Parse()
	root = flag.Arg(0)
	u, e := url.Parse(root)
	if e != nil {
		fmt.Println(e)
		return
	}
	check(u, u)
}
